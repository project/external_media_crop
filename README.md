# External Media Crop

Adds Field widget to upload files and images from third-party services with crop support.

## Installation

- Do the install steps for External Media and Image Widget Crop
- After enabling the module use the "External Media with Image Widget Crop" field widget on your image field.
